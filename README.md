# Hackgregator

This application is a comfortable reader application for news.ycombinator.com.
Its intended for the new Librem 5 smartphone from Purism but is not restricted to that.
Actually it is a Gtk application usable in every form factor. I try to keep that
convergence where its possible.

## Get it from Flathub

TBD

## Building and Installation

First make sure you have `libadwaita` installed.
Additionally you need:

* `glib`
* `gtk`
* `libsoup`
* `json-glib`
* `webkit2gtk6`

### Building and Testing native

```
$ meson build
$ ninja -C build
$ build/src/gui/hackgregator
```

### Building and Installing native

```
$ meson build
$ ninja -C build
$ sudo ninja -C build install
```

### Building and Running in sandbox

```
$ flatpak-builder build-dir hackgregator/build-aux/de.gunibert.Hackgregator.json
$ flatpak-builder --run build-dir hackgregator/build-aux/de.gunibert.Hackgregator.json hackgregator
```

How to create a flatpak bundle from that build can be read up on the flatpak
[reference](http://docs.flatpak.org/en/latest/first-build.html).

### Building with GNOME Builder (prefered)

Download this repository with GNOME Builder and click the execute button :-).
You can create a flatpak bundle directly from GNOME Builder too.

## Screenshots

![hackgregator landing](data/screenshots/hackgregator-landing.png)
![hackgregator article](data/screenshots/hackgregator-article.png)
![hackgregator comments](data/screenshots/hackgregator-comments.png)
![hackgregator mobile](data/screenshots/hackgregator-mobile.png)
