use crate::api::item::Item;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {

    use super::*;
    use gtk::glib::{ParamSpec, ParamSpecInt, ParamSpecString};
    use once_cell::sync::Lazy;

    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/de/gunibert/Hackgregator/commentsrow.ui")]
    pub struct CommentsRow {
        #[template_child]
        pub mainbox: TemplateChild<gtk::Box>,
        #[template_child]
        pub author: TemplateChild<gtk::Label>,
        #[template_child]
        pub text: TemplateChild<gtk::Label>,
        #[template_child]
        pub kids: TemplateChild<gtk::Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CommentsRow {
        const NAME: &'static str = "CommentsRow";
        type Type = super::CommentsRow;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl WidgetImpl for CommentsRow {}
    impl ObjectImpl for CommentsRow {
        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("author").build(),
                    ParamSpecString::builder("text").build(),
                    ParamSpecString::builder("kids").build(),
                    ParamSpecInt::builder("depth")
                        .minimum(0)
                        .write_only()
                        .build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                "author" => {
                    if let Ok(text) = value.get::<&str>() {
                        self.author.set_text(text);
                    }
                }
                "text" => {
                    let text = value.get::<&str>().expect("The value needs to be a string");
                    self.text.set_markup(text);
                }
                "kids" => {
                    let kids = value.get::<&str>().expect("The value needs to be a string");
                    if kids == "0" {
                        self.kids.hide();
                    } else if kids == "1" {
                        self.kids.set_label(&format!("{} direct comment", kids));
                    } else {
                        self.kids.set_label(&format!("{} direct comments", kids));
                    }
                }
                "depth" => {
                    let depth = value.get::<i32>().expect("The value needs to be a i32");
                    if depth > 0 {
                        self.kids.hide();
                    }
                    for _ in 0..depth {
                        let b = self.obj().create_comment_box();
                        self.mainbox.prepend(&b);
                    }
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                "author" => self.author.label().to_value(),
                "text" => self.text.label().to_value(),
                "kids" => self.kids.label().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct CommentsRow(ObjectSubclass<imp::CommentsRow>)
        @extends gtk::Widget;
}

impl CommentsRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn set_content(&self, content: &Item) {
        if let Some(text) = content.get_text() {
            // TODO: can we fetch a borrow here?
            self.imp().text.set_text(&text);
        }

        if let Some(author) = content.get_author() {
            self.imp().author.set_text(&author);
        }
    }

    fn create_comment_box(&self) -> gtk::Box {
        gtk::Box::builder()
            .width_request(5)
            .css_classes(vec!["commentline".to_string()])
            .build()
    }
}

impl Default for CommentsRow {
    fn default() -> Self {
        Self::new()
    }
}
