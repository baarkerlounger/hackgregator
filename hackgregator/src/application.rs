use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::api::item::ItemData;
use crate::config::VERSION;
use crate::HackregatorRustWindow;

pub enum Message {
    Stories(Vec<i64>),
    Item(ItemData),
    FetchedComments(ItemData),
    Comments(ItemData),
    CommentsTree(i64, Vec<ItemData>),
}

unsafe impl Send for Message {}

mod imp {

    use std::cell::RefCell;

    use glib::Sender;
    use gtk::glib::Receiver;

    use libadwaita::subclass::prelude::AdwApplicationImpl;

    use super::*;

    #[derive(Debug, Default)]
    pub struct HackregatorRustApplication {
        pub sender: RefCell<Option<Sender<Message>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HackregatorRustApplication {
        const NAME: &'static str = "HackregatorRustApplication";
        type Type = super::HackregatorRustApplication;
        type ParentType = libadwaita::Application;
    }

    impl HackregatorRustApplication {
        fn start_channel(&self, receiver: Receiver<Message>) {
            let obj = self.obj();
            receiver.attach(
                None,
                clone!(@weak obj => @default-return glib::ControlFlow::Break, move |msg| {
                    match msg {
                        Message::Stories(items) => {
                            if let Some(window) = obj.active_window() {
                                let window: HackregatorRustWindow = window
                                    .downcast()
                                    .expect("Could not cast window to HackregatorRustWindow");
                                window.set_stories(items);
                            }
                        }
                        Message::Item(itemdata) => {
                            if let Some(window) = obj.active_window() {
                                let window: HackregatorRustWindow = window
                                    .downcast()
                                    .expect("Could not cast window to HackregatorRustWindow");
                                window.update_item(itemdata);
                            }
                        }
                        Message::FetchedComments(itemdata) => {
                            if let Some(window) = obj.active_window() {
                                let window: HackregatorRustWindow = window
                                    .downcast()
                                    .expect("Could not cast window to HackregatorRustWindow");
                                crate::api::API.update_comments_in_db(&itemdata);
                                if let Some(kids) = itemdata.kids {
                                    // TODO: fix text
                                    window.imp().commentspage.set_comments(kids, None);
                                }
                            }
                        }
                        Message::Comments(itemdata) => {
                            if let Some(window) = obj.active_window() {
                                let window: HackregatorRustWindow = window
                                    .downcast()
                                    .expect("Could not cast window to HackregatorRustWindow");
                                window.update_comments(itemdata);
                            }
                        }
                        Message::CommentsTree(id, comments) => {
                            if let Some(window) = obj.active_window() {
                                let window: HackregatorRustWindow = window
                                    .downcast()
                                    .expect("Could not cast window to HackregatorRustWindow");
                                window.update_comments_tree(id, comments);
                            }
                        }
                    }
                    glib::ControlFlow::Continue
                }),
            );
        }
    }

    impl ObjectImpl for HackregatorRustApplication {
        fn constructed(&self) {
            self.parent_constructed();

            self.obj().setup_gactions();
            self.obj()
                .set_accels_for_action("app.quit", &["<primary>q"]);

            let (sender, receiver) = glib::MainContext::channel(glib::Priority::default());
            self.sender.borrow_mut().replace(sender);
            self.start_channel(receiver);
        }
    }

    impl ApplicationImpl for HackregatorRustApplication {
        fn activate(&self) {
            let application = self.obj();
            let gtk_app = application.upcast_ref::<gtk::Application>();
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = HackregatorRustWindow::new(
                    gtk_app,
                    self.sender
                        .borrow()
                        .as_ref()
                        .expect("No sender available")
                        .clone(),
                );
                window.upcast()
            };

            window.present();
        }
    }

    impl GtkApplicationImpl for HackregatorRustApplication {}
    impl AdwApplicationImpl for HackregatorRustApplication {}
}

glib::wrapper! {
    pub struct HackregatorRustApplication(ObjectSubclass<imp::HackregatorRustApplication>)
        @extends gio::Application, gtk::Application, libadwaita::Application,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl HackregatorRustApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        glib::Object::builder::<Self>()
            .property("application-id", &application_id)
            .property("flags", flags)
            .property("resource-base-path", &"/de/gunibert/Hackgregator")
            .build()
    }

    fn setup_gactions(&self) {
        let quit_action = gio::SimpleAction::new("quit", None);
        quit_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.quit();
        }));
        self.add_action(&quit_action);

        let about_action = gio::SimpleAction::new("about", None);
        about_action.connect_activate(clone!(@weak self as app => move |_, _| {
            app.show_about();
        }));
        self.add_action(&about_action);
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let dialog = gtk::AboutDialog::builder()
            .transient_for(&window)
            .modal(true)
            .logo_icon_name("de.gunibert.Hackgregator")
            .program_name("Hackgregator")
            .version(VERSION)
            .authors(vec!["Günther Wagner <info@gunibert.de>"])
            .artists(vec!["Robert Martinez <mail@mray.de>"])
            .copyright("© 2019-2023 Günther Wagner")
            .license_type(gtk::License::Gpl30)
            .build();

        dialog.present();
    }
}
