use crate::api::item::Item;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {

    use super::*;
    use gtk::glib::{ParamSpec, ParamSpecString};
    use once_cell::sync::Lazy;

    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/de/gunibert/Hackgregator/storyrow.ui")]
    pub struct StoryRow {
        #[template_child]
        pub tlbox: TemplateChild<gtk::Box>,
        #[template_child]
        pub title: TemplateChild<gtk::Label>,
        #[template_child]
        pub url: TemplateChild<gtk::Label>,
        #[template_child]
        pub comments: TemplateChild<gtk::Label>,
        #[template_child]
        pub url_dot: TemplateChild<gtk::Box>,
        #[template_child]
        pub comments_btn: TemplateChild<gtk::Button>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for StoryRow {
        const NAME: &'static str = "StoryRow";
        type Type = super::StoryRow;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl WidgetImpl for StoryRow {}
    impl ObjectImpl for StoryRow {
        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("title").build(),
                    ParamSpecString::builder("comments").build(),
                    ParamSpecString::builder("url").write_only().build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let text = value.get::<&str>().expect("The value needs to be a string");
                    self.title.set_text(text);
                }
                "comments" => {
                    let text = value.get::<&str>().expect("The value needs to be a string");
                    self.comments.set_text(text);
                }
                "url" => {
                    if value.get::<&str>().is_ok() {
                        let url = value.get::<&str>().unwrap();
                        self.url.set_text(url);
                        self.url.set_visible(true);
                        self.url_dot.style_context().add_class("dotgreen");
                        self.url_dot.style_context().remove_class("dotyellow");
                    } else {
                        self.url.set_visible(false);
                        self.url_dot.style_context().add_class("dotyellow");
                        self.url_dot.style_context().remove_class("dotgreen");
                    }
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            match pspec.name() {
                "title" => self.title.label().to_value(),
                "comments" => self.comments.label().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct StoryRow(ObjectSubclass<imp::StoryRow>)
        @extends gtk::Widget;
}

impl StoryRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn set_content(&self, content: &Item) {
        if let Some(title) = content.get_title() {
            // TODO: can we fetch a borrow here?
            self.imp().title.set_text(&title);
        }

        if let Some(descendants) = content.get_descendants() {
            self.imp().comments.set_text(&format!("{}", descendants));
        }
    }
}

impl Default for StoryRow {
    fn default() -> Self {
        Self::new()
    }
}
