pub static VERSION: &str = "0.5.0";
pub static GETTEXT_PACKAGE: &str = "hackgregator";
pub static LOCALEDIR: &str = "/app/share/locale";
pub static PKGDATADIR: &str = "/app/share/hackgregator";
pub static APPLICATION_ID: &str = "de.gunibert.Hackgregator";
