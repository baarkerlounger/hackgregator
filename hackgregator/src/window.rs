use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};
use libadwaita::subclass::prelude::*;

use crate::api::{
    item::{Item, ItemData},
    Category,
};
use crate::application::Message;
use glib::Sender;

#[derive(Clone, glib::Boxed, Debug)]
#[boxed_type(name = "SenderProp", nullable)]
pub struct SenderProp(pub glib::Sender<Message>);

mod imp {
    use std::{
        cell::{Cell, RefCell},
        collections::HashMap,
        str::FromStr,
    };

    use log::debug;

    use webkit6::traits::WebViewExt;

    use crate::commentspage::CommentsPage;
    use crate::{api::Category, storyrow::StoryRow};
    use glib::clone;
    use gtk::SingleSelection;
    use strum::IntoEnumIterator;

    use super::*;

    enum Pages {
        List,
        WebView,
        Comments,
    }

    impl From<Pages> for &str {
        fn from(pages: Pages) -> Self {
            match pages {
                Pages::List => "list",
                Pages::WebView => "web",
                Pages::Comments => "comments",
            }
        }
    }

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/de/gunibert/Hackgregator/window.ui")]
    pub struct HackregatorRustWindow {
        // Template widgets
        #[template_child]
        pub storylist: TemplateChild<gtk::ListView>,
        #[template_child]
        pub categories: TemplateChild<gtk::ComboBoxText>,
        #[template_child]
        pub progress: TemplateChild<gtk::ProgressBar>,
        #[template_child]
        pub content_stack: TemplateChild<libadwaita::Leaflet>,
        #[template_child]
        pub webview: TemplateChild<webkit6::WebView>,
        #[template_child]
        pub item_title: TemplateChild<libadwaita::WindowTitle>,
        #[template_child]
        pub comments_title: TemplateChild<libadwaita::WindowTitle>,
        #[template_child]
        pub commentspage: TemplateChild<CommentsPage>,

        pub progress_items: Cell<i64>,

        pub items: RefCell<HashMap<i64, Item>>,

        url: RefCell<Option<String>>,

        pub api: crate::api::Api,

        pub sender: RefCell<Option<SenderProp>>,
    }

    #[gtk::template_callbacks]
    impl HackregatorRustWindow {
        #[template_callback]
        pub fn refresh_stories(&self) {
            if let Some(active_text) = self.categories.active_text() {
                let category =
                    Category::from_str(&active_text).expect("Could not determine Category");
                self.obj().fetch_stories(category);
            }
        }

        #[template_callback]
        pub fn go_back(&self) {
            self.content_stack
                .set_visible_child_name(Pages::List.into());

            self.webview.stop_loading();
            self.webview.load_uri("about:blank");
        }

        #[template_callback]
        pub fn open_in_browser(&self) {
            let url = self.url.borrow();
            if let Some(url) = url.as_ref() {
                match gio::AppInfo::launch_default_for_uri(url, gio::AppLaunchContext::NONE) {
                    Ok(()) => log::debug!("Launching url=\"{url}\""),
                    Err(err) => log::debug!("Can't launch url=\"{url}\": {err}"),
                }
            }
        }

        #[template_callback]
        pub fn show_article(&self) {
            self.storylist.model().and_then(|model| {
                let single_selection_model = model.downcast::<SingleSelection>().ok()?;
                let item = single_selection_model
                    .selected_item()?
                    .downcast::<Item>()
                    .ok()?;
                let url = item.get_url()?;
                debug!("Url: {url}");
                self.content_stack
                    .set_visible_child_name(Pages::WebView.into());
                self.item_title.set_subtitle(&item.get_title().unwrap());
                self.webview.load_uri(&url);
                Some(())
            });
        }

        #[template_callback]
        pub fn category_changed(&self, cb: gtk::ComboBoxText) {
            if let Some(active_text) = cb.active_text() {
                let category =
                    Category::from_str(active_text.as_str()).expect("Could not determine Category");
                self.obj().fetch_stories(category);
            }
        }

        fn show_comments_page(&self, listitem: gtk::ListItem, _text: Option<String>) {
            let i = listitem
                .item()
                .expect("No content found")
                .downcast::<Item>()
                .expect("Cant downcast to Item");
            self.content_stack
                .set_visible_child_name(Pages::Comments.into());
            if let Some(title) = i.get_title() {
                self.comments_title.set_subtitle(&title);
            }

            self.commentspage.clear();

            let sender = self.sender.borrow().as_ref().unwrap().clone();
            let id = i.get_id();
            crate::RUNTIME.spawn(async move {
                let item = crate::api::API.get_item(id).await;
                if let Ok(item) = item {
                    sender.0.send(Message::FetchedComments(item)).unwrap();
                }
            });
        }

        fn create_list_click_controller(&self, listitem: &gtk::ListItem) -> gtk::GestureClick {
            let click_controller = gtk::GestureClick::builder().button(1).build();
            click_controller.set_propagation_phase(gtk::PropagationPhase::Capture);
            let s = self;
            click_controller.connect_released(glib::clone!(@weak listitem, @weak s => move |controller, _n_press, x, y| {
                let story_row = controller.widget().downcast::<StoryRow>().expect("Cant downcast to StoryRow");
                if let Some(widget) = story_row.pick(x, y, gtk::PickFlags::DEFAULT) {
                    let i = listitem.item().expect("No content found").downcast::<Item>().expect("Cant downcast to Item");
                    debug!("Picked {:?}", widget);
                    if widget == story_row.imp().comments_btn.get() ||
                       widget.parent().unwrap().parent().unwrap() == story_row.imp().comments_btn.get() ||
                       widget.type_().is_a(gtk::Image::static_type()) {
                        debug!("Showing comments page. Picked: {:?}", widget);
                        s.show_comments_page(listitem, None);
                    } else {
                        debug!("Showing web page");
                        if let Some(url) = i.get_url() {
                            debug!("Url: {url} ({i:?})");
                            s.content_stack.set_visible_child_name(Pages::WebView.into());
                            s.item_title.set_subtitle(&i.get_title().unwrap());
                            s.webview.load_uri(&url);

                            s.url.borrow_mut().replace(url);
                        } else if let Some(text) = i.get_text() {
                            debug!("Showing text");
                            s.show_comments_page(listitem, Some(text));
                        }
                    }
                }
            }));
            click_controller
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HackregatorRustWindow {
        const NAME: &'static str = "HackregatorRustWindow";
        type Type = super::HackregatorRustWindow;
        type ParentType = libadwaita::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for HackregatorRustWindow {
        fn constructed(&self) {
            self.parent_constructed();

            //if crate::config::APPLICATION_ID.ends_with(".Devel") {
            //self.obj().add_css_class("devel");
            //}

            self.obj().restore_window_state();

            let categories = Category::iter();

            for (i, c) in categories.enumerate() {
                self.categories.append_text(c.to_user_string());
                if c == Category::Top {
                    self.categories.set_active(Some(i as u32));
                }
            }

            let factory = gtk::SignalListItemFactory::new();
            factory.connect_bind(|_factory, item| {
                let row = item
                    .child()
                    .and_then(|child| child.downcast::<StoryRow>().ok())
                    .expect("Can't get StoryRow");
                let content = item
                    .item()
                    .and_then(|item| item.downcast::<Item>().ok())
                    .expect("Can't get Item");

                content.bind_property("title", &row, "title").build();
                content
                    .bind_property("descendants", &row, "comments")
                    .transform_to(|_, val: glib::Value| {
                        let num = val.get::<i64>().unwrap();
                        Some(format!("{}", num).to_value())
                    })
                    .transform_from(|_, val: glib::Value| {
                        let comments = val.get::<&str>().unwrap();
                        i64::from_str(comments).map(|i| i.to_value()).ok()
                    })
                    .build();
                content.bind_property("url", &row, "url").build();
            });

            let obj = self.obj();
            factory.connect_setup(clone!(@weak obj => move |_factory, listitem| {
                let row = StoryRow::new();
                let click_controller = obj.imp().create_list_click_controller(listitem);

                row.add_controller(click_controller);
                listitem.set_child(Some(&row));
            }));

            self.storylist.set_factory(Some(&factory));
        }

        // fn properties() -> &'static [glib::ParamSpec] {
        //     static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
        //         vec![glib::ParamSpecBoxed::builder::<glib::ParamSpec>("sender").build()]
        //     });
        //
        //     PROPERTIES.as_ref()
        // }
        //
        // fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        //     match pspec.name() {
        //         "sender" => {
        //             let sender = value.get::<&SenderProp>().expect("Can't get SenderProp");
        //             self.sender.replace(Some(sender.to_owned()));
        //         }
        //         _ => unimplemented!(),
        //     }
        // }
    }

    impl WidgetImpl for HackregatorRustWindow {}
    impl WindowImpl for HackregatorRustWindow {}
    impl ApplicationWindowImpl for HackregatorRustWindow {}
    impl AdwApplicationWindowImpl for HackregatorRustWindow {}
}

glib::wrapper! {
    pub struct HackregatorRustWindow(ObjectSubclass<imp::HackregatorRustWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow,
        @implements gio::ActionGroup, gio::ActionMap;
}

impl HackregatorRustWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P, sender: Sender<Message>) -> Self {
        let sender_clone = sender.clone();
        let obj: Self = glib::Object::builder()
            .property("application", application)
            .build();
        obj.imp().sender.replace(Some(SenderProp(sender)));
        obj.imp().commentspage.set_sender(sender_clone);
        obj.imp().refresh_stories();

        obj
    }

    pub fn fetch_stories(&self, category: Category) {
        let sender_borrow = self.imp().sender.borrow();
        if let Some(sender) = sender_borrow.as_ref() {
            let sender = sender.clone();
            crate::RUNTIME.spawn(async move {
                let items = crate::api::API.get_stories(category, 100).await;
                if let Ok(items) = items {
                    sender.0.send(Message::Stories(items)).unwrap();
                }
            });
        }
    }

    pub fn fetch_item(&self, items: Vec<i64>) {
        let sender = self.imp().sender.borrow().as_ref().unwrap().clone();
        crate::RUNTIME.spawn(async move {
            for chunk in items.chunks(10) {
                let items = crate::api::API.get_items(chunk.to_vec()).await;
                for item in items {
                    sender.0.send(Message::Item(item)).unwrap();
                }
            }
        });
    }

    pub fn set_stories(&self, stories: Vec<i64>) {
        self.imp().items.borrow_mut().clear();
        self.imp().progress_items.set(0);
        self.imp().progress.set_visible(true);
        self.imp().progress.set_fraction(0.0f64);
        let model = gio::ListStore::new::<Item>();
        stories.iter().for_each(|id| {
            let item = Item::new(*id);
            self.imp().items.borrow_mut().insert(*id, item.clone());
            model.append(&item);
        });
        self.imp()
            .storylist
            .set_model(Some(&gtk::SingleSelection::new(Some(model))));

        self.fetch_item(stories);
    }

    pub fn update_item(&self, itemdata: ItemData) {
        let map = self.imp().items.borrow();
        if let Some(item) = map.get(&itemdata.id) {
            item.set_item_data(itemdata);
        }

        self.imp()
            .progress_items
            .set(self.imp().progress_items.get() + 1);

        self.imp().progress.set_fraction(
            self.imp().progress_items.get() as f64 / self.imp().items.borrow().len() as f64,
        );

        if self.imp().progress_items.get() == self.imp().items.borrow().len() as i64 {
            self.imp().progress.set_visible(false);
        }
    }

    pub fn update_comments(&self, itemdata: ItemData) {
        self.imp().commentspage.update_comments(itemdata);
    }

    pub fn update_comments_tree(&self, id: i64, comments: Vec<ItemData>) {
        self.imp().commentspage.update_comments_tree(id, comments);
    }

    fn restore_window_state(&self) {
        // FIXME: https://gitlab.gnome.org/GNOME/gtk/-/issues/4136
        // let settings = utils::settings_manager();
        // let width = settings.int("window-width");
        // let height = settings.int("window-height");
        // self.set_default_size(width, height);
        self.set_default_size(720, 605);
    }
}
