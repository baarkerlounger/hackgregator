use crate::api::item::ItemData;
use crate::schema::items;
use chrono::{DateTime, Local};
use diesel::{prelude::*, Insertable, Queryable};

#[derive(Queryable, Insertable, AsChangeset, Selectable, Identifiable, Debug)]
pub struct Item {
    pub id: i32,
    pub type_: Option<String>,
    pub title: Option<String>,
    pub text: Option<String>,
    pub url: Option<String>,
    pub by: Option<String>,
    pub time: Option<DateTime<Local>>,
    pub score: Option<i32>,
    pub comments: Option<i32>,
}

impl From<&ItemData> for Item {
    fn from(itemdata: &ItemData) -> Self {
        Self {
            id: itemdata.id as i32,
            type_: itemdata.itemtype.as_ref().map(|t| t.into()),
            title: itemdata.title.clone(),
            text: itemdata.text.clone(),
            url: itemdata.url.clone(),
            by: itemdata.by.clone(),
            time: itemdata.time,
            score: itemdata.score.map(|score| score as i32),
            comments: itemdata.descendants.map(|descendants| descendants as i32),
        }
    }
}

impl From<Item> for ItemData {
    fn from(value: Item) -> Self {
        Self {
            id: value.id as i64,
            text: value.text,
            title: value.title,
            url: value.url,
            by: value.by,
            time: value.time,
            score: value.score.map(|score| score as i64),
            descendants: value.comments.map(|comments| comments as i64),
            ..Default::default()
        }
    }
}
