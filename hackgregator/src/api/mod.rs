use libadwaita::glib;
use std::cell::RefCell;
use std::{fmt::Debug, str::FromStr};

use crate::db::items::Item;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;
use diesel::Connection;
use diesel_migrations::embed_migrations;
use diesel_migrations::EmbeddedMigrations;
use diesel_migrations::MigrationHarness;
use futures::{stream, Future, StreamExt};
use item::ItemData;
use itemstore::ItemStore;
use log::debug;
use once_cell::sync::Lazy;
use petgraph::visit::{Dfs, Walker};
use reqwest::Client;
use strum::EnumIter;

pub mod item;
pub mod itemstore;

pub(crate) static API: Lazy<Api> =
    Lazy::new(|| Api::new("https://hacker-news.firebaseio.com/v0/".to_string()));

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

#[derive(Debug, PartialEq, EnumIter)]
pub enum Category {
    Top,
    Best,
    New,
    Ask,
    Show,
    Job,
}

impl Category {
    fn get_url(&self) -> &'static str {
        match self {
            Category::Top => "topstories.json",
            Category::Best => "beststories.json",
            Category::New => "newstories.json",
            Category::Ask => "askstories.json",
            Category::Show => "showstories.json",
            Category::Job => "jobstories.json",
        }
    }

    pub fn to_user_string(&self) -> &'static str {
        match self {
            Category::Top => "Top Stories",
            Category::Best => "Best Stories",
            Category::New => "New Stories",
            Category::Ask => "Ask HN",
            Category::Show => "Show HN",
            Category::Job => "Jobs",
        }
    }
}

impl FromStr for Category {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Top Stories" => Ok(Self::Top),
            "New Stories" => Ok(Self::New),
            "Ask HN" => Ok(Self::Ask),
            "Show HN" => Ok(Self::Show),
            "Best Stories" => Ok(Self::Best),
            "Jobs" => Ok(Self::Job),
            _ => Err(()),
        }
    }
}

#[derive(Default, Debug)]
pub struct Api {
    baseurl: String,
    client: Client,
    itemstore: RefCell<ItemStore>,
}

unsafe impl Send for Api {}
unsafe impl Sync for Api {}

impl Api {
    pub fn new(baseurl: String) -> Self {
        let s = Self {
            baseurl,
            client: Client::new(),
            itemstore: RefCell::new(ItemStore::new()),
        };

        let mut conn = s.get_connection();
        conn.run_pending_migrations(MIGRATIONS).unwrap();

        s
    }

    pub async fn get_items(&self, item_ids: Vec<i64>) -> Vec<ItemData> {
        let (known_items, items_to_fetch): (Vec<_>, Vec<_>) = item_ids
            .iter()
            .partition(|id| self.itemstore.borrow().contains_item(id));

        // first only fetch unseen items
        let known_itemdata: Vec<ItemData> = known_items
            .into_iter()
            .map(|id| self.itemstore.borrow().get_item(&id))
            .collect();

        // next fetch items from the db
        let db_itemdata: Vec<ItemData> = items_to_fetch
            .iter()
            .filter_map(|id| self.load_item_from_db(*id))
            .collect();

        debug!("{db_itemdata:?}");

        let items_to_fetch: Vec<_> = items_to_fetch
            .into_iter()
            .filter(|id| !db_itemdata.iter().any(|item| item.id == *id))
            .collect();

        let res: Vec<Result<ItemData, reqwest::Error>> = self
            .get_stories_stream(&items_to_fetch)
            .buffered(5)
            .collect()
            .await;
        let mut fetched_items: Vec<ItemData> =
            res.into_iter().filter_map(|item| item.ok()).collect();
        let mut store = self.itemstore.borrow_mut();
        for item in fetched_items.iter() {
            store.add_item(item.clone());
        }
        fetched_items.extend(known_itemdata);
        fetched_items.extend(db_itemdata);

        // just save the items to the db
        for i in &fetched_items {
            self.save_item_to_db(i);
        }

        fetched_items
    }

    pub fn save_item_to_db(&self, itemdata: &ItemData) {
        let mut conn = self.get_connection();

        let item_to_store: crate::db::items::Item = itemdata.into();
        diesel::insert_into(crate::schema::items::table)
            .values(&item_to_store)
            .on_conflict(crate::schema::items::id)
            .do_update()
            .set(&item_to_store)
            .execute(&mut conn)
            .expect("Could not save into db");
    }

    fn get_connection(&self) -> SqliteConnection {
        let mut user_data_dir = glib::user_data_dir();
        user_data_dir.push("hackgregator");
        std::fs::create_dir_all(&user_data_dir).unwrap();
        user_data_dir.push("items.db");

        let conn = SqliteConnection::establish(&user_data_dir.to_string_lossy()).unwrap();
        conn
    }

    pub fn load_item_from_db(&self, item_id: i64) -> Option<ItemData> {
        use crate::schema::items::dsl::*;
        let mut conn = self.get_connection();

        items
            .find(item_id as i32)
            .first(&mut conn)
            .map(|item: Item| {
                debug!("{item:?}");
                item.into()
            })
            .ok()
    }

    pub fn update_comments_in_db(&self, item: &ItemData) {
        use crate::schema::items::dsl::*;

        let db_item: crate::db::items::Item = item.into();

        let mut conn = self.get_connection();
        diesel::update(items.find(db_item.id))
            .set(comments.eq(&db_item.comments))
            .execute(&mut conn)
            .expect("Error on Update");
    }

    pub async fn get_stories(
        &self,
        category: Category,
        len: usize,
    ) -> Result<Vec<i64>, reqwest::Error> {
        let res: Vec<i64> = self
            .client
            .get(format!("{}/{}", self.baseurl, category.get_url()))
            .send()
            .await?
            .json()
            .await?;

        if res.len() > len {
            Ok(res[..len].to_vec())
        } else {
            Ok(res)
        }
    }

    pub async fn get_comments(&self, kids: Vec<i64>) -> Vec<ItemData> {
        let mut graph = petgraph::Graph::<ItemData, ()>::new();
        let root = graph.add_node(ItemData::default());
        self.get_subtree(&mut graph, root, kids, 1).await;

        let dfs = Dfs::new(&graph, root);
        let list: Vec<ItemData> = dfs
            .iter(&graph)
            .map(|visited| graph[visited].clone())
            .collect();
        list[1..].to_vec()
    }

    pub async fn get_item(&self, id: i64) -> Result<ItemData, reqwest::Error> {
        let resp = self
            .client
            .get(format!("{}/item/{}.json", self.baseurl, id))
            .send()
            .await?;
        resp.json().await
    }

    #[async_recursion::async_recursion]
    async fn get_subtree(
        &self,
        graph: &mut petgraph::Graph<ItemData, ()>,
        root: petgraph::graph::NodeIndex,
        kids: Vec<i64>,
        depth: i32,
    ) {
        let items = self.get_items(kids).await;
        for mut item in items {
            item.depth = depth;
            // let kid_item = self.get_item(kid).await.unwrap();
            let next_kids = item.kids.clone();
            let child = graph.add_node(item);
            graph.add_edge(root, child, ());

            if let Some(kids) = next_kids {
                self.get_subtree(graph, child, kids, depth + 1).await;
            }
        }
    }

    fn get_stories_stream<'a>(
        &'a self,
        stories: &'a [i64],
    ) -> impl stream::Stream<Item = impl Future<Output = Result<ItemData, reqwest::Error>> + 'a> + 'a
    {
        stream::iter(stories).map(|id| self.get_item(*id))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use claim::*;
    use mockito::mock;

    fn get_api(url: Option<String>) -> Api {
        if let Some(url) = url {
            Api::new(url)
        } else {
            Api::new("https://hacker-news.firebaseio.com/v0/".to_string())
        }
    }

    #[tokio::test]
    async fn get_stories_success() {
        let m = mock("GET", "/newstories.json")
            .with_status(200)
            .with_body("[1234]")
            .create();
        let url = mockito::server_url();

        let api = get_api(Some(url));
        let stories = api.get_stories(Category::New, 30).await;

        m.assert();
        if let Ok(stories) = stories {
            assert_eq!(stories.len(), 1);
        }
    }

    #[tokio::test]
    async fn get_stories_failure() {
        let m = mock("GET", "/newstories.json").with_status(500).create();
        let url = mockito::server_url();

        let api = get_api(Some(url));
        let stories = api.get_stories(Category::New, 30).await;

        m.assert();
        assert_err!(stories);
    }

    #[tokio::test]
    async fn get_item_integration_test() {
        let api = get_api(None);
        let item = api.get_item(8863).await.expect("Could not fetch item");

        assert_eq!(item.id, 8863);
        assert_eq!(
            item.title,
            Some("My YC app: Dropbox - Throw away your USB drive".to_string())
        );
    }

    #[tokio::test]
    async fn get_stories_integration_test() {
        let api = get_api(None);

        let stories = api.get_stories(Category::Best, 30).await;

        if let Ok(stories) = stories {
            assert_eq!(stories.len(), 30);
        }
    }

    #[tokio::test]
    async fn test_get_comments() {
        let api = get_api(None);

        let item = api.get_item(8863).await.unwrap();
        let list = api.get_comments(item.kids.unwrap()).await;

        for i in list {
            println!(" {:?}", i);
        }
    }
}
