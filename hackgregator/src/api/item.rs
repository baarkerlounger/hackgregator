use chrono::{DateTime, Local, Utc};
use glib::ObjectExt;
use glib::ParamSpecBuilderExt;
use gtk::glib;
use gtk::subclass::prelude::*;
use serde::{Deserialize, Deserializer};

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "lowercase")]
pub enum Type {
    Story,
    Job,
    Comment,
    Poll,
    PollOpt,
}

impl From<&Type> for String {
    fn from(itemtype: &Type) -> Self {
        match itemtype {
            Type::Story => "story".to_string(),
            Type::Job => "job".to_string(),
            Type::Comment => "comment".to_string(),
            Type::Poll => "poll".to_string(),
            Type::PollOpt => "pollopt".to_string(),
        }
    }
}

#[derive(Deserialize, Debug, Default, Clone)]
pub struct ItemData {
    pub id: i64,
    #[serde(deserialize_with = "deserialize_text", default)]
    pub text: Option<String>,
    pub title: Option<String>,
    pub by: Option<String>,
    pub url: Option<String>,
    pub descendants: Option<i64>,
    pub kids: Option<Vec<i64>>,
    pub deleted: Option<bool>,
    pub dead: Option<bool>,
    #[serde(deserialize_with = "deserialize_datetime")]
    pub time: Option<DateTime<Local>>,
    pub score: Option<i64>,
    pub parent: Option<i64>,
    #[serde(rename = "type")]
    pub itemtype: Option<Type>,
    #[serde(skip)]
    pub depth: i32,
}

fn deserialize_datetime<'de, D>(deserializer: D) -> Result<Option<DateTime<Local>>, D::Error>
where
    D: Deserializer<'de>,
{
    let unix: i64 = Deserialize::deserialize(deserializer)?;
    let naive = chrono::NaiveDateTime::from_timestamp_opt(unix, 0);
    let dt: Option<DateTime<Utc>> =
        naive.map(|naive| chrono::DateTime::from_naive_utc_and_offset(naive, Utc));
    let dt = dt.map(|dt| dt.with_timezone(&Local));

    Ok(dt)
}

fn deserialize_text<'de, D>(deserializer: D) -> Result<Option<String>, D::Error>
where
    D: Deserializer<'de>,
{
    let text: String = Deserialize::deserialize(deserializer)?;
    if let Ok(text) = escaper::decode_html_sloppy(&text) {
        return Ok(Some(text));
    }
    Ok(Some(text))
}

mod imp {
    use glib::{ParamSpec, ParamSpecInt, ParamSpecInt64, ParamSpecString, ToValue};
    use once_cell::sync::Lazy;

    use super::*;
    use std::cell::{Cell, RefCell};

    #[derive(Default, Debug)]
    pub struct Item {
        pub id: Cell<i64>,
        pub data: RefCell<ItemData>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Item {
        const NAME: &'static str = "Item";
        type Type = super::Item;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for Item {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecInt64::builder("id").build(),
                    ParamSpecString::builder("title").build(),
                    ParamSpecString::builder("text").build(),
                    ParamSpecInt64::builder("descendants").build(),
                    ParamSpecInt::builder("kids").minimum(0).read_only().build(),
                    ParamSpecString::builder("url").build(),
                    ParamSpecString::builder("author").build(),
                    ParamSpecInt::builder("depth").minimum(0).build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &glib::Value, pspec: &ParamSpec) {
            match pspec.name() {
                "id" => {
                    let id = value.get::<i64>().unwrap();
                    self.id.set(id);
                }
                "title" => {
                    let title = value.get::<&str>().unwrap();
                    self.data.borrow_mut().title.replace(title.to_string());
                }
                "text" => {
                    let text = value.get::<&str>().unwrap();
                    self.data.borrow_mut().text.replace(text.to_string());
                }
                "descendants" => {
                    let descendants = value.get::<i64>().unwrap();
                    self.data.borrow_mut().descendants.replace(descendants);
                }
                "url" => {
                    let url = value.get::<&str>().unwrap();
                    self.data.borrow_mut().url.replace(url.to_string());
                }
                "author" => {
                    let author = value.get::<&str>().unwrap();
                    self.data.borrow_mut().by.replace(author.to_string());
                }
                "depth" => {
                    let depth = value.get::<i32>().unwrap();
                    self.data.borrow_mut().depth = depth;
                }
                _ => todo!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> glib::Value {
            let obj = self.obj();
            match pspec.name() {
                "id" => self.id.get().to_value(),
                "title" => obj
                    .get_title()
                    .unwrap_or_else(|| "...".to_string())
                    .to_value(),
                "text" => obj
                    .get_text()
                    .unwrap_or_else(|| "...".to_string())
                    .to_value(),
                "descendants" => obj.get_descendants().unwrap_or_default().to_value(),
                "url" => obj.get_url().to_value(),
                "author" => obj.get_author().unwrap_or_default().to_value(),
                "depth" => obj.get_depth().to_value(),
                "kids" => {
                    let count = if let Some(kids) = self.data.borrow().kids.as_ref() {
                        kids.len() as i32
                    } else {
                        0
                    };
                    count.to_value()
                }
                _ => todo!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct Item(ObjectSubclass<imp::Item>);
}

unsafe impl Send for Item {}

impl Item {
    pub fn new(id: i64) -> Self {
        glib::Object::builder::<Self>().property("id", &id).build()
    }

    pub fn set_item_data(&self, item: ItemData) {
        self.imp().data.replace(item);
        self.notify("title");
        self.notify("descendants");
        self.notify("url");
        self.notify("text");
        self.notify("author");
        self.notify("depth");
        self.notify("kids");
    }

    pub fn notify_data(&self) {
        self.notify("title");
        self.notify("descendants");
        self.notify("url");
        self.notify("text");
        self.notify("author");
        self.notify("depth");
        self.notify("kids");
    }

    pub fn get_id(&self) -> i64 {
        self.imp().data.borrow().id
    }

    pub fn get_title(&self) -> Option<String> {
        self.imp().data.borrow().title.clone()
    }

    pub fn get_text(&self) -> Option<String> {
        self.imp().data.borrow().text.clone()
    }

    pub fn get_url(&self) -> Option<String> {
        self.imp().data.borrow().url.clone()
    }

    pub fn get_descendants(&self) -> Option<i64> {
        self.imp().data.borrow().descendants
    }

    pub fn get_kids(&self) -> Option<Vec<i64>> {
        self.imp().data.borrow().kids.clone()
    }

    pub fn get_dead(&self) -> Option<bool> {
        self.imp().data.borrow().dead
    }

    pub fn get_deleted(&self) -> Option<bool> {
        self.imp().data.borrow().deleted
    }

    pub fn get_author(&self) -> Option<String> {
        self.imp().data.borrow().by.clone()
    }

    pub fn get_depth(&self) -> i32 {
        self.imp().data.borrow().depth
    }
}
