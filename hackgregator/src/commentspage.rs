use crate::api::item::{Item, ItemData};

use crate::application::Message;

use gtk::gio;
use gtk::glib::Sender;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use gtk::{glib, SelectionModel};

mod imp {
    use super::*;
    use crate::api::Api;
    use std::sync::Arc;
    use std::{cell::RefCell, collections::HashMap};

    #[derive(CompositeTemplate, Default)]
    #[template(resource = "/de/gunibert/Hackgregator/commentspage.ui")]
    pub struct CommentsPage {
        pub sender: RefCell<Option<Sender<Message>>>,
        #[template_child]
        pub commentslist: TemplateChild<gtk::ListView>,

        pub items: RefCell<HashMap<i64, Item>>,

        #[template_child]
        pub text_label: TemplateChild<gtk::Label>,

        pub api: Arc<RefCell<Api>>,
    }

    #[gtk::template_callbacks]
    impl CommentsPage {
        #[template_callback]
        fn comment_clicked(&self, pos: u32) {
            if let Some(model) = self.commentslist.model() {
                if let Some(item) = model.item(pos) {
                    let item = item.downcast::<Item>().unwrap();
                    let is_child = item.get_depth() > 0;

                    let already_revealed = if let Some(next_item) = model.item(pos + 1) {
                        let next_item = next_item.downcast::<Item>().unwrap();
                        next_item.get_depth() > 0
                    } else {
                        false
                    };

                    if is_child || already_revealed {
                        return;
                    }
                    self.obj().fetch_comments_tree(item);
                    // log::debug!("Comment {} clicked", item.get_id());
                }
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CommentsPage {
        const NAME: &'static str = "CommentsPage";
        type Type = super::CommentsPage;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            Self::bind_template_callbacks(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl WidgetImpl for CommentsPage {}
    impl ObjectImpl for CommentsPage {
        fn constructed(&self) {
            self.parent_constructed();

            let factory = gtk::SignalListItemFactory::new();
            factory.connect_setup(|_factory, item| {
                let row = crate::commentsrow::CommentsRow::new();
                item.set_child(Some(&row));
            });
            factory.connect_bind(|_factory, item| {
                let row = item
                    .child()
                    .expect("No child found")
                    .downcast::<crate::commentsrow::CommentsRow>()
                    .expect("Cant downcast to CommentsRow");
                let content = item
                    .item()
                    .expect("No content found")
                    .downcast::<Item>()
                    .expect("Cant downcast to Item");

                content
                    .bind_property("text", &row, "text")
                    .transform_to(|_, val: glib::Value| {
                        let string = val.get::<&str>().unwrap();
                        let pangostring = html2pango::markup_html(string).unwrap();
                        Some(pangostring.to_value())
                    })
                    .build();
                content
                    .bind_property("kids", &row, "kids")
                    .transform_to(|_, val: glib::Value| {
                        let kids = val.get::<i32>().unwrap();
                        Some(format!("{}", kids).to_value())
                    })
                    .build();
                content.bind_property("author", &row, "author").build();
                content.bind_property("depth", &row, "depth").build();
            });
            self.commentslist.set_factory(Some(&factory));
        }

        fn dispose(&self) {
            while let Some(child) = self.obj().first_child() {
                child.unparent();
            }
        }
    }
}

glib::wrapper! {
    pub struct CommentsPage(ObjectSubclass<imp::CommentsPage>)
        @extends gtk::Widget;
}

impl CommentsPage {
    pub fn new(sender: Sender<Message>) -> Self {
        let obj: Self = glib::Object::new();
        obj.set_sender(sender);

        obj
    }

    pub fn set_sender(&self, sender: Sender<Message>) {
        self.imp().sender.borrow_mut().replace(sender);
    }

    pub fn clear(&self) {
        self.imp().commentslist.set_model(SelectionModel::NONE);
    }

    pub fn set_comments(&self, comments: Vec<i64>, text: Option<String>) {
        log::debug!("Set Comments.");
        if let Some(text) = text {
            self.imp().text_label.set_visible(true);
            self.imp().text_label.set_text(&text);
        } else {
            self.imp().text_label.set_visible(false);
        }

        let model = gio::ListStore::new::<Item>();
        comments.iter().for_each(|id| {
            let item = Item::new(*id);
            self.imp().items.borrow_mut().insert(*id, item.clone());
            model.append(&item);
        });

        let filter = gtk::CustomFilter::new(|item| {
            let item = item.downcast_ref::<Item>();
            if let Some(item) = item {
                if let Some(dead) = item.get_dead() {
                    return !dead;
                }
                if let Some(deleted) = item.get_deleted() {
                    return !deleted;
                }
            }
            true
        });

        let filtermodel = gtk::FilterListModel::builder()
            .model(&model)
            .filter(&filter)
            .build();

        self.imp()
            .commentslist
            .set_model(Some(&gtk::SingleSelection::new(Some(filtermodel))));
        self.fetch_comments(comments);
    }

    pub fn fetch_comments(&self, items: Vec<i64>) {
        log::debug!("Fetch {} comments.", items.len());
        let sender = self.imp().sender.borrow().as_ref().unwrap().clone();
        crate::RUNTIME.spawn(async move {
            for chunk in items.chunks(10) {
                let items = crate::api::API.get_items(chunk.to_vec()).await;
                for item in items {
                    sender.send(Message::Comments(item)).unwrap();
                }
            }
        });
    }

    pub fn fetch_comments_tree(&self, item: Item) {
        let id = item.get_id();
        let kids = item.get_kids();
        let sender = self.imp().sender.borrow().as_ref().unwrap().clone();
        crate::RUNTIME.spawn(async move {
            if let Some(kids) = kids {
                let list = crate::api::API.get_comments(kids).await;
                sender.send(Message::CommentsTree(id, list)).unwrap();
            }
        });
    }

    pub fn update_comments(&self, itemdata: ItemData) {
        let deleted = itemdata.deleted.is_some();
        let dead = itemdata.dead.is_some();

        let map = self.imp().items.borrow();
        if let Some(item) = map.get(&itemdata.id) {
            log::debug!("Set Item Data {:?}", itemdata);
            item.set_item_data(itemdata);
        }

        if deleted || dead {
            let selectionmodel: gtk::SingleSelection = self
                .imp()
                .commentslist
                .model()
                .expect("Could not get selection model")
                .downcast::<gtk::SingleSelection>()
                .unwrap();
            let filtermodel: gtk::FilterListModel = selectionmodel
                .model()
                .expect("Could not get filter model")
                .downcast()
                .unwrap();
            filtermodel
                .filter()
                .unwrap()
                .changed(gtk::FilterChange::Different);
        }
    }

    pub fn update_comments_tree(&self, id: i64, comments: Vec<ItemData>) {
        let pos = self.find_position_of_item_with_id(id) + 1;

        let comments: Vec<Item> = comments
            .into_iter()
            .map(|data| {
                let item = Item::new(data.id);
                item.set_item_data(data);
                item
            })
            .collect();

        let selectionmodel = self
            .imp()
            .commentslist
            .model()
            .unwrap()
            .downcast::<gtk::SingleSelection>()
            .unwrap();

        let filtermodel: gtk::FilterListModel = selectionmodel
            .model()
            .unwrap()
            .downcast::<gtk::FilterListModel>()
            .unwrap();

        let store = filtermodel
            .model()
            .unwrap()
            .downcast::<gio::ListStore>()
            .unwrap();

        store.splice(pos, 0, &comments);
        for i in comments {
            i.notify_data();
        }
    }

    fn find_position_of_item_with_id(&self, id: i64) -> u32 {
        let model = self.imp().commentslist.model().unwrap();
        let mut pos = 0u32;

        loop {
            let i = model.item(pos);
            match i {
                Some(item) => {
                    let item = item.downcast::<Item>().unwrap();
                    if item.get_id() == id {
                        return pos;
                    }
                }
                None => break,
            }
            pos += 1;
        }
        unimplemented!("This should never happen");
    }
}
